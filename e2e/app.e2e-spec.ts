import { AngRedditPage } from './app.po';

describe('ang-reddit App', () => {
  let page: AngRedditPage;

  beforeEach(() => {
    page = new AngRedditPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
